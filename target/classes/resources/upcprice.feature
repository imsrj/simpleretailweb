Feature: Ensure each UPC has Correct Price
Validate each UPC has corresponding correct price on
shipment page.

@Web
Scenario: UPC and Price Validation
	Given the following Data:
	|567321101987	  | 19.99 |
	|567321101986	  | 17.99 |
	|567321101985	  | 20.49 |
	|567321101984	  | 23.88 |
	|467321101899	  | 9.75  |
	|477321101878	  | 17.25 | 
	
	When User Navigates to create Shipment Page
	
	Then each UPC should have a corresponding correct price from the page