Feature: Populate Drop Down menu with Employee List worked less than 40 hours.

@Web
Scenario: Verify DropDown Menu Population

Given The index page

When User coming on Shipment page
	
Then the dropdown menu should be populated with list of employees who worked less than forty hours.