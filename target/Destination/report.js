$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("employeedropdown.feature");
formatter.feature({
  "line": 1,
  "name": "Populate Drop Down menu with Employee List worked less than 40 hours.",
  "description": "",
  "id": "populate-drop-down-menu-with-employee-list-worked-less-than-40-hours.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 5035901551,
  "status": "passed"
});
formatter.before({
  "duration": 3789283441,
  "status": "passed"
});
formatter.before({
  "duration": 3804358594,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify DropDown Menu Population",
  "description": "",
  "id": "populate-drop-down-menu-with-employee-list-worked-less-than-40-hours.;verify-dropdown-menu-population",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "The index page",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "User coming on Shipment page",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "the dropdown menu should be populated with list of employees who worked less than forty hours.",
  "keyword": "Then "
});
formatter.match({
  "location": "EmployeeDropDownStepDefinition.the_index_page()"
});
formatter.result({
  "duration": 1349692491,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeDropDownStepDefinition.user_coming_on_Shipment_page()"
});
formatter.result({
  "duration": 25981009,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeDropDownStepDefinition.the_dropdown_menu_should_be_populated_with_list_of_employees_who_worked_less_than_forty_hours()"
});
formatter.result({
  "duration": 99834174,
  "status": "passed"
});
formatter.after({
  "duration": 743620660,
  "status": "passed"
});
formatter.after({
  "duration": 772145284,
  "status": "passed"
});
formatter.after({
  "duration": 911136835,
  "status": "passed"
});
formatter.uri("upcprice.feature");
formatter.feature({
  "line": 1,
  "name": "Ensure each UPC has Correct Price",
  "description": "Validate each UPC has corresponding correct price on\r\nshipment page.",
  "id": "ensure-each-upc-has-correct-price",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4577912683,
  "status": "passed"
});
formatter.before({
  "duration": 3691464970,
  "status": "passed"
});
formatter.before({
  "duration": 3948889423,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "UPC and Price Validation",
  "description": "",
  "id": "ensure-each-upc-has-correct-price;upc-and-price-validation",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "the following Data:",
  "rows": [
    {
      "cells": [
        "567321101987",
        "19.99"
      ],
      "line": 8
    },
    {
      "cells": [
        "567321101986",
        "17.99"
      ],
      "line": 9
    },
    {
      "cells": [
        "567321101985",
        "20.49"
      ],
      "line": 10
    },
    {
      "cells": [
        "567321101984",
        "23.88"
      ],
      "line": 11
    },
    {
      "cells": [
        "467321101899",
        "9.75"
      ],
      "line": 12
    },
    {
      "cells": [
        "477321101878",
        "17.25"
      ],
      "line": 13
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "User Navigates to create Shipment Page",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "each UPC should have a corresponding correct price from the page",
  "keyword": "Then "
});
formatter.match({
  "location": "UPCPriceStepDefinition.the_following_Data(DataTable)"
});
formatter.result({
  "duration": 5436194,
  "status": "passed"
});
formatter.match({
  "location": "UPCPriceStepDefinition.user_Navigates_to_create_Shipment_Page()"
});
formatter.result({
  "duration": 1073047241,
  "status": "passed"
});
formatter.match({
  "location": "UPCPriceStepDefinition.each_UPC_should_have_a_corresponding_correct_price_from_the_page()"
});
formatter.result({
  "duration": 377745876,
  "status": "passed"
});
formatter.after({
  "duration": 665607571,
  "status": "passed"
});
formatter.after({
  "duration": 773231452,
  "status": "passed"
});
formatter.after({
  "duration": 690099451,
  "status": "passed"
});
formatter.uri("validateemployeeid.feature");
formatter.feature({
  "line": 1,
  "name": "Validate Employee Id in Shipment Confirmation Page",
  "description": "",
  "id": "validate-employee-id-in-shipment-confirmation-page",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3704909532,
  "status": "passed"
});
formatter.before({
  "duration": 3703140159,
  "status": "passed"
});
formatter.before({
  "duration": 4233240484,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify Employee Id",
  "description": "",
  "id": "validate-employee-id-in-shipment-confirmation-page;verify-employee-id",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "The Index url",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "User come on create shipment page, checks shipment checkboxes and click create shipment button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "employee id should match the dropdown menu employee id",
  "keyword": "Then "
});
formatter.match({
  "location": "EmployeeIdStepDefinition.the_Index_url()"
});
formatter.result({
  "duration": 1226250454,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeIdStepDefinition.user_come_on_create_shipment_page_checks_shipment_checkboxes_and_click_create_shipment_button()"
});
formatter.result({
  "duration": 10421527721,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeIdStepDefinition.employee_id_should_match_the_dropdown_menu_employee_id()"
});
formatter.result({
  "duration": 79905628,
  "status": "passed"
});
formatter.after({
  "duration": 681803699,
  "status": "passed"
});
formatter.after({
  "duration": 700104778,
  "status": "passed"
});
formatter.after({
  "duration": 797366779,
  "status": "passed"
});
});