package com.retail.core;

import java.util.ArrayList;
import java.util.List;

public class EmployeeList {
  private List<Employee> employeeList = new ArrayList<Employee>();
  
  public EmployeeList(){

		 employeeList.add(new Employee(1, "Sarang", "Jaltare", 35));
		 employeeList.add(new Employee(2, "Justin", "Poole", 38));
		 employeeList.add(new Employee(3, "Sukanth", "Gunda", 42));
		 employeeList.add(new Employee(4, "Vishal", "Agrawal", 45));

	  
  }
  
  public List<Employee> getEmployees(){
		
		return employeeList;
		
	}
        
    public List<Employee> getEmployeesWhoWorkedLessThan40(){
         
    	List<Employee> employeeLessThan40List = new ArrayList<Employee>();
    	
    	for(int i=0; i<employeeList.size(); i++) {
    		 Employee temp = employeeList.get(i);
    		if(employeeList.get(i).getHoursWorked()<40)
    			employeeLessThan40List.add(temp);
    	}
    	return employeeLessThan40List;
    }
}
