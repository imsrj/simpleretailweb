package com.retail.core;

public class Employee {
     private int id;
     private String firstName;
     private String lastName;
     private int hoursWorked;
     
     public Employee(int id, String firstName, String lastName, int hoursWorked){
    	 this.setId(id);
    	 this.setFirstName(firstName);
    	 this.setLastName(lastName);
    	 this.setHoursWorked(hoursWorked);
     }
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getHoursWorked() {
		return hoursWorked;
	}
	public void setHoursWorked(int hoursWorked) {
		this.hoursWorked = hoursWorked;
	}
     
     
}
