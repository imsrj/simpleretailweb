package com.retail.web;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.retail.core.Employee;
import com.retail.core.EmployeeList;

public class EmployeeTest {

	 
	 Employee employee ;
	@Before
	public void setUp() {
		employee = new Employee(1, "sarang", "jaltare", 4);
		
	}
	@Test
	public void getEmployeeIdTest() {
		  assertEquals(1, employee.getId());
	}
	
	@Test
	public void getEmployeeFirstNameTest() {
		 assertEquals("sarang", employee.getFirstName());
	}
	
	@Test
	public void getEmployeeLastNameTest() {
		 assertEquals("jaltare", employee.getLastName());
	}
	@Test
	public void getHoursWorkedTest() {
		 assertEquals(4, employee.getHoursWorked());
	}


}
