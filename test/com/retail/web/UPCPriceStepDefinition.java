package com.retail.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UPCPriceStepDefinition {
	private ChromeDriver driver;
	private Map<String,Double>  upcPriceDatabase;

	@Before("@Web")
	public void setUp() {


String os = System.getProperty("os.name").toLowerCase().split(" ")[0];
	
	if("mac".contains(os)) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "/chromedriver");
		
		
	}else {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
	}
	 driver = new ChromeDriver();
	}

@Given("^the following Data:$")
public void the_following_Data(DataTable upcPriceTable) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
    // E,K,V must be a scalar (String, Integer, Date, enum etc)
   //throw new PendingException();
	
	upcPriceDatabase= upcPriceTable.asMap(String.class, Double.class);
	for (String key : upcPriceDatabase.keySet()) {
		   System.out.println("UPC: " + key + " price: " + upcPriceDatabase.get(key));
		}
	System.out.println(upcPriceDatabase.size());
}

@When("^User Navigates to create Shipment Page$")
public void user_Navigates_to_create_Shipment_Page() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	driver.get("http://localhost:8080/SimpleRetailWeb_starter/index.html");
	String xpathVal = "//input[@value='Get Items To Be Shipped']";
	WebElement btn = driver.findElement(By.xpath(xpathVal));
	btn.submit();
	assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));
}

@Then("^each UPC should have a corresponding correct price from the page$")
public void each_UPC_should_have_a_corresponding_correct_price_from_the_page() throws Throwable {
   
	List<WebElement> upcFromPageList = driver.findElements(By.xpath("//tbody/tr/td[2]"));
	List<WebElement> priceFromPageList = driver.findElements(By.xpath("//tbody/tr/td[4]"));

   //Actual Comparision between price at our Map Database for each UPC and Price at UI for each UPC
	//Here, UPC from UI is passed as key to our Database Map
	// Corresponding to that key(if exists), the value(price) gets returned from our database map
	// That value(price) is being compared to price list we got from UI.
	for (int i=0 ; i<upcPriceDatabase.size(); i++) {
		   //System.out.println("weight: " + key + " price: " + weightList.get(key));
			  //assert(weightList.containsKey(key), weightPriceMapFromUI.get(tdPrice), .001);
			  assertEquals(upcPriceDatabase.get(upcFromPageList.get(i).getText()), Double.parseDouble(priceFromPageList.get(i).getText()), 0.001);
		}
}

@After("@Web")
	public void tearDown() {
		driver.close();
		driver.quit();
	}
}
