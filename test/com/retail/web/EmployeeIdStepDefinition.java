package com.retail.web;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EmployeeIdStepDefinition {
	private ChromeDriver driver;
	private Map<String,Double>  upcPriceDatabase;

	@Before("@Web")
	public void setUp() {


String os = System.getProperty("os.name").toLowerCase().split(" ")[0];
	
	if("mac".contains(os)) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "/chromedriver");
		
		
	}else {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
	}
	 driver = new ChromeDriver();
	}

	

@Given("^The Index url$")
public void the_Index_url() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	driver.get("http://localhost:8080/SimpleRetailWeb_starter/index.html");
	String xpathVal = "//input[@value='Get Items To Be Shipped']";
	WebElement btn = driver.findElement(By.xpath(xpathVal));
	btn.submit();
}


@When("^User come on create shipment page, checks shipment checkboxes and click create shipment button$")
public void user_come_on_create_shipment_page_checks_shipment_checkboxes_and_click_create_shipment_button() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	
	assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));
	 WebElement checkBox = driver.findElement(By.id("checkedRows"));
	 checkBox.click();
	 WebElement createShipmentButton = driver.findElement(By.xpath("/html/body/form/input"));
	 createShipmentButton.click();
	 assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));
	 
	
}

@Then("^employee id should match the dropdown menu employee id$")
public void employee_id_should_match_the_dropdown_menu_employee_id() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
   /// throw new PendingException();
	WebElement element = driver.findElement(By.tagName("h3"));
   assertTrue(element.getText().contains("1"));
}





@After("@Web")
	public void tearDown() {
		driver.close();
		driver.quit();
	}

}
