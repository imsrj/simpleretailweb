package com.retail.web;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EmployeeDropDownStepDefinition {
	private ChromeDriver driver;

	@Before("@Web")
	public void setUp() {

		String os = System.getProperty("os.name").toLowerCase().split(" ")[0];

		if ("mac".contains(os)) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver");

		} else {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");
		}
		driver = new ChromeDriver();
		//System.out.println("Driver value " + driver == null);
	}

@Given("^The index page$")
public void the_index_page() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
   // throw new PendingException();
	driver.get("http://localhost:8080/SimpleRetailWeb_starter/index.html");
	String xpathVal = "//input[@value='Get Items To Be Shipped']";
	WebElement btn = driver.findElement(By.xpath(xpathVal));
	btn.submit();
	
}


	@When("^User coming on Shipment page$")
	public void user_coming_on_Shipment_page() throws Throwable {
    /* System.out.println("\n***" + driver.getCurrentUrl());
		driver.get("http://localhost:8080/SimpleRetailWeb_starter");
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();

		assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));*/
		assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));
		
	}

	@Then("^the dropdown menu should be populated with list of employees who worked less than forty hours\\.$")
	public void the_dropdown_menu_should_be_populated_with_list_of_employees_who_worked_less_than_forty_hours()
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
		WebElement dropdownOption = driver.findElement(By.name("option"));
		assertEquals("Sarang", dropdownOption.getText());
	}

@After("@Web")
	public void tearDown() {
		driver.close();
		driver.quit();
	}
}
