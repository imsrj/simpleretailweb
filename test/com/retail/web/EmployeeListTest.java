package com.retail.web;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.retail.core.Employee;
import com.retail.core.EmployeeList;

public class EmployeeListTest {
     
	 EmployeeList myEmployeeList ;
	@Before
	public void setUp() {
		myEmployeeList = new EmployeeList();
		
	}
	@Test
	public void getEmployeeListTest() {
		assertNotNull(myEmployeeList.getEmployees());
	}
	
	@Test
	public void getEmployeeListLessThan40Test() {
		assertNotNull(myEmployeeList.getEmployeesWhoWorkedLessThan40());
	}

}
